// =============================================================================
// Grunt Config ================================================================
// =============================================================================
// Wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function(grunt) {

  // ===========================================================================
  // LOAD GRUNT PLUGINS ========================================================
  // ===========================================================================
  // Loaded if they are in our package.json.
  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

  // ===========================================================================
  // CONFIGURE GRUNT ===========================================================
  // ===========================================================================
  grunt.initConfig({
      // Get the configuration info from package.json
      pkg: grunt.file.readJSON('package.json'),

      watch: {
          files: ['app/src/assets/less/*.less'],
          tasks: ['less']
      },
      less: {
          dev: {
              files: {
                "app/dist/assets/css/main.css" : "app/src/assets/less/*.less"
              }
          }
      },
      browserSync: {
          bsFiles: {
            src: ['app/index.html', 'app/dist/assets/css/*.css']
          },
          options: {
              watchTask: true,
              server: {
                baseDir: "./app"
              },
              port: 9610
          }
      }
  });



  // ===========================================================================
  // CREATE TASKS ==============================================================
  // ===========================================================================
  grunt.registerTask('default', ['browserSync', 'watch']);
};
